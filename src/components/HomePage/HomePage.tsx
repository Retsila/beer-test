import { Grid } from '@material-ui/core'
import Pagination from '@material-ui/lab/Pagination'
import BeerList from '../BeerList'
import { useQuery } from 'react-query'
import { Beer, BeerWsApi } from '../../services/codegenClient'
import { PaginationItem } from '@material-ui/lab'
import { Link } from 'react-router-dom'

interface HomePageProps {
  page: number
}

export const skeletons = (arraySize: number): Beer[] =>
  Array.from({ length: arraySize }, (value, id) => {
    return {
      id: id * -1,
      name: 'name',
      description: 'description',
      tagline: 'tagline',
      image_url: 'imageUrl',
      brewers_tips: 'tips',
      ingredients: {
        malt: [],
        hops: [],
        yeast: 'yeast',
      },
      volume: {
        value: 10,
        unit: 'unit',
      },
    }
  })

const HomePage = (props: HomePageProps): JSX.Element => {
  const { page } = props
  const Api = new BeerWsApi()

  const beersResult = useQuery(
    ['getBeers', page],
    () => Api.getBeers(page, 9),
    {
      staleTime: 1000 * 60 * 10,
    }
  )

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      style={{ marginTop: '1%' }}
    >
      <Grid item container justifyContent="center">
        <BeerList
          loading={beersResult.isLoading}
          beers={beersResult?.data?.data ? beersResult.data.data : skeletons(9)}
        />
      </Grid>
      <Grid item style={{ marginTop: '1%' }}>
        <Pagination
          count={36}
          page={page}
          renderItem={(pageItem) => (
            <PaginationItem
              component={Link}
              to={`/home?page=${pageItem.page}`}
              {...pageItem}
            />
          )}
        />
      </Grid>
    </Grid>
  )
}

export default HomePage

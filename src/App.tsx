import React, { useContext } from 'react'
import './App.css'
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom'
import { Button, ButtonGroup, Grid, Typography } from '@material-ui/core'
import HomePage from './components/HomePage'
import { MyContext, MyContextType } from './ConfigApp'
import CartPage from './components/CartPage'
import DescriptionPage from './components/DescriptionPage/DescriptionPage'

function App() {
  const useQuery = () => {
    const { search } = useLocation()
    return React.useMemo(() => new URLSearchParams(search), [search])
  }
  const { pathname } = useLocation()
  let navigate = useNavigate()
  const query = useQuery()
  const { cart }: MyContextType = useContext(MyContext) as MyContextType

  const links = [
    <Button onClick={() => navigate('/home?page=1')} key="homePageButtonKey">
      Home Page
    </Button>,
    <Button onClick={() => navigate('/cart')} key="cartPageButtonKey">
      Cart Page (Number of items: {cart.length})
    </Button>,
  ]

  const getPageNumber = (): number => {
    const queryPage = query.get('page')
    if (typeof queryPage === 'string') {
      const pageNumber = parseInt(queryPage)
      if (!isNaN(pageNumber)) {
        return pageNumber <= 0 ? 1 : pageNumber >= 37 ? 36 : pageNumber
      }
    }
    return 1
  }

  const getBeerId = (): number => {
    const queryId = query.get('id')
    if (typeof queryId === 'string') {
      const beerId = parseInt(queryId)
      if (!isNaN(beerId)) {
        return beerId
      }
    }
    return 0
  }

  return (
    <>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        style={{ padding: '2%' }}
        direction="column"
      >
        <Grid item container justifyContent="space-between" alignItems="center">
          <Typography variant="h1">
            {pathname === '/home'
              ? 'HOME'
              : pathname === '/cart'
              ? 'CART'
              : 'DESC'}
          </Typography>
          <ButtonGroup>{links}</ButtonGroup>
        </Grid>
        <Routes>
          <Route
            path="/home"
            element={<HomePage page={getPageNumber()} />}
          ></Route>
          <Route path="/cart" element={<CartPage />}></Route>
          <Route
            path="/desc"
            element={<DescriptionPage beerId={getBeerId()} />}
          ></Route>
        </Routes>
      </Grid>
    </>
  )
}

export default App

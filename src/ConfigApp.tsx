import React, { Dispatch, useEffect, useState } from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'
import { sessionStorageSafeGet, sessionStorageSafeSet } from './utils/utils'
import { sessionStorageCartKey } from './constants'

const queryClient = new QueryClient()

export interface MyContextType {
  cart: Number[]
  setCart: Dispatch<any>
}
export const MyContext = React.createContext<MyContextType | null>(null)

const ConfigApp = (): JSX.Element => {
  const [cart, setCart] = useState<Number[]>([])

  useEffect(() => {
    if (cart.length === 0) {
      const beerIdsInSessionStorage = sessionStorageSafeGet(
        sessionStorageCartKey
      )
      if (beerIdsInSessionStorage) {
        const beerIdsJson: Number[] = JSON.parse(beerIdsInSessionStorage)
        if (beerIdsJson.length > 0) {
          setCart(beerIdsJson)
        }
      }
    }
  }, [])

  useEffect(() => {
    sessionStorageSafeSet(sessionStorageCartKey, JSON.stringify(cart))
  }, [cart])

  return (
    <QueryClientProvider client={queryClient}>
      <MyContext.Provider
        value={{
          cart: cart,
          setCart: setCart,
        }}
      >
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </MyContext.Provider>
    </QueryClientProvider>
  )
}

export default ConfigApp

import { makeStyles } from '@material-ui/core'

export const useStyles = makeStyles((theme) => ({
  cardContainer: {
    width: '100%',
    height: 48,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  textContainer: {
    width: '100%',
    height: 48,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '16px',
  },
  caption: {
    fontWeight: 400,
    fontSize: '10px',
    lineHeight: '16px',
  },
  cardContentContainer: {
    '&:hover': {
      cursor: 'pointer',
    },
    padding: '5%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '&:last-child': {
      paddingBottom: 0,
    },
  },
  img: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: 194,
    marginBottom: 8,
    objectFit: 'contain',
  },
}))

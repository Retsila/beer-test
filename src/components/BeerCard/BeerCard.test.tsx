import React from 'react'
import { render, screen } from '@testing-library/react'
import BeerCard from './BeerCard'
import { Beer } from '../../services/codegenClient'
import { logRoles } from '@testing-library/dom'

import { MyContext } from '../../ConfigApp'
import { BrowserRouter } from 'react-router-dom'

const mockCart: Number[] = []
const mockSetCart = jest.fn()

const ContextWrapper =
  (cart?: Number[]) =>
  (props: { children: JSX.Element }): React.ReactElement =>
    (
      <MyContext.Provider
        value={{ cart: cart || mockCart, setCart: mockSetCart }}
      >
        <BrowserRouter>
          {/* eslint-disable-next-line testing-library/no-node-access */}
          {props.children}
        </BrowserRouter>
      </MyContext.Provider>
    )

const beerMock: Beer = {
  id: 1,
  name: 'beerMock',
  tagline: 'tagline',
  description: 'description',
  image_url: 'url',
  volume: {
    value: 100,
    unit: 'gramme',
  },
  brewers_tips: 'tips',
  ingredients: {
    malt: [
      {
        name: 'malt1',
        amount: {
          value: 11,
          unit: 'gramme',
        },
      },
      {
        name: 'malt2',
        amount: {
          value: 12,
          unit: 'gramme',
        },
      },
    ],
    hops: [
      {
        name: 'hops1',
        amount: {
          value: 21,
          unit: 'gramme',
        },
      },
      {
        name: 'hops2',
        amount: {
          value: 22,
          unit: 'gramme',
        },
      },
    ],
    yeast: 'yeast',
  },
}

describe('BeerCard', () => {
  test('Simple render', () => {
    const { container } = render(<BeerCard loading={false} beer={beerMock} />, {
      wrapper: ContextWrapper(),
    })
    expect(screen.getByRole('img', { name: 'beerMock' })).toBeInTheDocument()
    expect(screen.getByText('tagline')).toBeInTheDocument()
    expect(screen.getByRole('button')).toHaveStyle('color: green')
    logRoles(container)
  })
  test('Simple render with loading', () => {
    render(<BeerCard loading={true} beer={beerMock} />, {
      wrapper: ContextWrapper(),
    })
    expect(screen.getByRole('progressbar')).toBeInTheDocument()
    expect(
      screen.queryByRole('img', { name: 'beerMock' })
    ).not.toBeInTheDocument()
    expect(screen.queryByText('tagline')).not.toBeInTheDocument()
  })
  test('Render with desc', () => {
    render(<BeerCard loading={false} beer={beerMock} isDescription={true} />, {
      wrapper: ContextWrapper(),
    })
    expect(screen.getByRole('img', { name: 'beerMock' })).toBeInTheDocument()
    expect(screen.getByText('tagline')).toBeInTheDocument()
    expect(screen.getByText('description')).toBeInTheDocument()
    expect(screen.getByText('tips')).toBeInTheDocument()
  })
  test('Render with red button', () => {
    render(<BeerCard loading={false} beer={beerMock} isDescription={true} />, {
      wrapper: ContextWrapper([1]),
    })
    expect(screen.getByRole('button')).toHaveStyle('color: red')
  })
})

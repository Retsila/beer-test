openapi: 3.0.0

info:
  title: "Services for Beer-test"
  description: "API for test project"
  version: 1.0.0
  contact:
    name: "Alister GRACIAS"
    email: "alllister.gracias@gmail.com"

servers:
  - url: 'https://api.punkapi.com/v2'
    description: "Beer server"

tags:
  - name: "BeerWs"
    description: "Beer web services"

paths:
  /beers:
    get:
      tags:
        - BeerWs
      operationId: getBeers
      description: 'Get list of beers'
      parameters:
        - in: query
          name: page
          schema:
            type: number
          description: 'Return x element on designated page'
        - in: query
          name: per_page
          schema:
            type: number
          description: 'Return number of element on page x'
        - in: query
          name: beer_name
          schema:
            type: string
          description: 'Returns all beers matching the supplied name (this will match partial strings as well so e.g punk will return Punk IPA), if you need to add spaces just add an underscore (_).'
        - in: query
          name: ids
          schema:
            type: string
          description: "Returns all beers matching the supplied ID's. You can pass in multiple ID's by separating them with a | symbol."
      responses:
        200:
          description: "List of beers"
          content:
            application/json:
              schema:
                type: array
                description: 'Beers'
                minItems: 1
                items:
                  $ref: '#/components/schemas/Beer'

components:
  schemas:
    Beer:
      type: object
      description: "Beer type"
      properties:
        id:
          type: number
        name:
          type: string
        tagline:
          type: string
        description:
          type: string
        image_url:
          type: string
        volume:
          type: object
          properties:
            value:
              type: number
            unit:
              type: string
          required:
            - value
            - unit
        brewers_tips:
          type: string
        ingredients:
          $ref: '#/components/schemas/BeerIngredients'
      required:
        - id
        - name
        - tagline
        - description
        - image_url
        - volume
        - brewers_tips
        - ingredients

    BeerIngredients:
      type: object
      description: 'Beer ingredients'
      properties:
        malt:
          type: array
          description: 'Constitution of malt'
          items:
            $ref: '#/components/schemas/IngredientConstitution'
        hops:
          type: array
          description: 'Constitution of hops'
          items:
            $ref: '#/components/schemas/IngredientConstitution'
        yeast:
          type: string
      required:
        - malt
        - hops
        - yeast

    IngredientConstitution:
      type: object
      description: 'Ingredient (malt or hops) constitution'
      properties:
        name:
          type: string
        amount:
          type: object
          properties:
            value:
              type: number
            unit:
              type: string
          required:
            - value
            - unit
        add:
          type: string
        attribute:
          type: string
      required:
        - name
        - amount

import { Typography } from '@material-ui/core'
import { IngredientConstitution } from '../../services/codegenClient'

interface DisplayIngredientsProps {
  ingredient: IngredientConstitution[] | undefined
  name: string
}
const DisplayIngredients = (props: DisplayIngredientsProps): JSX.Element => {
  const { ingredient, name } = props
  return (
    <div>
      <Typography display="inline">{name}: </Typography>
      {ingredient &&
        ingredient.map((ing) => (
          <Typography
            key={ing.name}
            style={{ fontStyle: 'italic', display: 'inline' }}
            variant="caption"
          >
            {ing.name}({ing.amount.value}
            {ing.amount.unit})
          </Typography>
        ))}
    </div>
  )
}

export default DisplayIngredients

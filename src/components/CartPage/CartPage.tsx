import { MyContext, MyContextType } from '../../ConfigApp'
import { useContext, useEffect, useState } from 'react'
import { Grid } from '@material-ui/core'
import BeerList from '../BeerList'
import { useQuery } from 'react-query'
import { Beer, BeerWsApi } from '../../services/codegenClient'
import { skeletons } from '../HomePage/HomePage'

const constructIdsStringQuery = (ids: Number[]): string => ids.join('|')

const CartPage = (): JSX.Element => {
  const { cart }: MyContextType = useContext(MyContext) as MyContextType
  const Api = new BeerWsApi()
  const [cartLoaded, setCartLoaded] = useState<Beer[]>([])

  const idsQuery = constructIdsStringQuery(cart)

  const beersResult = useQuery(
    ['getBeers', idsQuery],
    () => Api.getBeers(undefined, undefined, undefined, idsQuery),
    {
      staleTime: 1000 * 60 * 10,
      enabled:
        cart.length > 0 &&
        (cartLoaded.length === 0 ||
          cart.every((id) => !cartLoaded.find((cart: Beer) => id === cart.id))),
    }
  )

  useEffect(() => {
    if (beersResult?.data?.data) {
      setCartLoaded(beersResult.data.data)
    }
  }, [beersResult.isLoading, beersResult?.data?.data])

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      style={{ marginTop: '1%' }}
    >
      <Grid item container justifyContent="center">
        <BeerList
          loading={beersResult.isLoading}
          beers={
            beersResult.isLoading
              ? skeletons(cart.length)
              : cartLoaded.filter((c) => cart.find((i) => i === c.id))
          }
        />
      </Grid>
    </Grid>
  )
}

export default CartPage

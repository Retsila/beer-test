// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const sessionStorageSafeSet = (key: string, obj: any): void => {
  try {
    sessionStorage.setItem(key, obj)
  } catch {
    // Don't do anything
  }
}

export const sessionStorageSafeRemove = (key: string): void => {
  try {
    sessionStorage.removeItem(key)
  } catch {
    // Don't do anything
  }
}

export const sessionStorageSafeGet = (key: string): string | null => {
  try {
    return sessionStorage.getItem(key)
  } catch {
    return null
  }
}

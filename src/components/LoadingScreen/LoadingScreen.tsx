import React from 'react'
import { Grid, CircularProgress, GridSpacing } from '@material-ui/core'

const LoadingScreen = (props: {
  children?: JSX.Element
  spacing?: GridSpacing
  style?: React.CSSProperties
  size?: number | string
}): JSX.Element => {
  const { spacing, style, children, size } = props
  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      spacing={spacing ? spacing : 4}
      style={{ ...style }}
    >
      <Grid item>
        <CircularProgress size={size} />
      </Grid>
      {children && <Grid item>{children}</Grid>}
    </Grid>
  )
}

export default LoadingScreen

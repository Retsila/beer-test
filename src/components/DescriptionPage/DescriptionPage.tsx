import { Grid, Typography } from '@material-ui/core'
import { useQuery } from 'react-query'
import { BeerWsApi } from '../../services/codegenClient'
import BeerCard from '../BeerCard'
import { useStyles } from './DescriptionPage.style'

interface DescriptionPageProps {
  beerId: number
}

const DescriptionPage = (props: DescriptionPageProps): JSX.Element => {
  const { beerId } = props
  const Api = new BeerWsApi()
  const beerResult = useQuery(
    ['getBeer', beerId],
    () => Api.getBeers(undefined, undefined, undefined, beerId.toString()),
    {
      staleTime: 1000 * 60 * 10,
    }
  )
  const classes = useStyles()
  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      style={{ marginTop: '1%' }}
    >
      <Grid item xs={12} className={classes.cardContainer}>
        {!beerId || (!beerResult.isLoading && !beerResult?.data?.data?.[0]) ? (
          <Typography>Error loading description page</Typography>
        ) : (
          <BeerCard
            isDescription={true}
            loading={beerResult.isLoading}
            beer={beerResult.data?.data[0]}
          />
        )}
      </Grid>
    </Grid>
  )
}

export default DescriptionPage

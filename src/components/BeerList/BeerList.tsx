import { Grid } from '@material-ui/core'
import BeerCard from '../BeerCard'
import { Beer } from '../../services/codegenClient'

interface BeerListProps {
  loading: boolean
  beers: Beer[]
}

const BeerList = (props: BeerListProps): JSX.Element => {
  const { loading, beers } = props
  return (
    <Grid container direction="row" spacing={2} style={{ width: '100%' }}>
      {beers.map((beer) => (
        <Grid key={beer.id} item xs={4} style={{ width: '33%' }}>
          <BeerCard loading={loading} beer={beer} />
        </Grid>
      ))}
    </Grid>
  )
}

export default BeerList

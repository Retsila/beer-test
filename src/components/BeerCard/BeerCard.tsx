import { Card, CardContent, IconButton, Typography } from '@material-ui/core'
import LoadingScreen from '../LoadingScreen'
import { useStyles } from './BeerCard.style'
import Skeleton from '@material-ui/lab/Skeleton'
import { Beer } from '../../services/codegenClient'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart'
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart'
import { useContext } from 'react'
import { MyContext, MyContextType } from '../../ConfigApp'
import { useNavigate } from 'react-router-dom'
import DisplayIngredients from '../DisplayIngredients'

interface BeerCardProps {
  isDescription?: boolean
  loading: boolean
  beer: Beer | undefined
}

const BeerCard = (props: BeerCardProps): JSX.Element => {
  const { loading, beer, isDescription } = props
  const { cart, setCart }: MyContextType = useContext(
    MyContext
  ) as MyContextType
  const isPresentInCart = cart.find((id) => id === beer?.id)
  const classes = useStyles()

  let navigate = useNavigate()
  return (
    <Card elevation={2} onClick={() => navigate(`/desc?id=${beer?.id}`)}>
      <CardContent
        onClick={() => console.log('card')}
        className={classes.cardContentContainer}
      >
        {loading ? (
          <div className={classes.img}>
            <LoadingScreen style={{ display: 'flex', alignItems: 'center' }} />
          </div>
        ) : (
          <img className={classes.img} alt={beer?.name} src={beer?.image_url} />
        )}

        {loading ? (
          <div className={classes.cardContainer}>
            <Skeleton width="80%" />
            <Skeleton width="60%" />
          </div>
        ) : (
          <div className={classes.textContainer}>
            <Typography className={classes.title}>
              {`${beer?.name} -- ${beer?.volume.value} ${beer?.volume.unit}`}
            </Typography>
            <Typography className={classes.caption}>{beer?.tagline}</Typography>
          </div>
        )}
        {isDescription && loading ? (
          <div className={classes.cardContainer}>
            <Skeleton width="100%" />
            <Skeleton width="100%" />
            <Skeleton width="100%" />
          </div>
        ) : (
          isDescription && (
            <div style={{ alignSelf: 'flex-start', width: '100%' }}>
              <Typography>
                <b>Description: </b>
                {beer?.description}
              </Typography>
              <div>
                <Typography style={{ fontWeight: 'bold' }}>
                  Ingredients:
                </Typography>
                <div>
                  <DisplayIngredients
                    name="Malt"
                    ingredient={beer?.ingredients.malt}
                  />
                  <DisplayIngredients
                    name="Hops"
                    ingredient={beer?.ingredients.hops}
                  />
                </div>
              </div>
              <Typography>
                <b>How to brew: </b>
                {beer?.brewers_tips}
              </Typography>
            </div>
          )
        )}
        <IconButton
          onClick={(e) => {
            e.stopPropagation()
            if (!isPresentInCart) {
              setCart((oldCart: Number[]) => [...oldCart, beer?.id])
            } else {
              setCart((oldCart: Number[]) =>
                oldCart.filter((id) => id !== beer?.id)
              )
            }
          }}
          style={{ color: isPresentInCart ? 'red' : 'green' }}
        >
          {isPresentInCart ? (
            <RemoveShoppingCartIcon />
          ) : (
            <AddShoppingCartIcon />
          )}
        </IconButton>
      </CardContent>
    </Card>
  )
}

export default BeerCard
